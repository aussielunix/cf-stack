/**
 * Outputs
 */

// The VPC ID
output "id" {
  value = "${aws_vpc.main.id}"
}

// A comma-separated list of subnet IDs.
output "external_subnets" {
  value = ["${aws_subnet.external.*.id}"]
}

// A list of private subnet IDs.
output "private_subnets" {
  value = ["${aws_subnet.private.*.id}"]
}

// A list of services subnet IDs.
output "services_subnets" {
  value = ["${aws_subnet.services.*.id}"]
}

// A list of rds subnet IDs.
output "rds_subnets" {
  value = ["${aws_subnet.rds.*.id}"]
}

// The default VPC security group ID.
output "security_group" {
  value = "${aws_vpc.main.default_security_group_id}"
}

// The list of availability zones of the VPC.
output "availability_zones" {
  value = ["${aws_subnet.external.*.availability_zone}"]
}

// The internal route table ID.
output "internal_rtb_id" {
  value = "${join(",", aws_route_table.internal.*.id)}"
}

// The external route table ID.
output "external_rtb_id" {
  value = "${aws_route_table.external.id}"
}
