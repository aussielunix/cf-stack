/**
 * The cf-stack module combines sub modules to create a complete
 * cloudfoundry ready stack with vpc, nat gateways, subnets,
 * securiitygroups and a bastion node that enables you to
 * access the bosh director.
 *
 * Usage:
 *
 *    module "cf-stack" {
 *      source      = "bitbucket.org/aussielunix/cf-stack"
 *      name        = "mystack"
 *      environment = "prod"
 *      key_name    = "mystackkey"
 *    }
 *
 */

variable "acl" {
  default = "private"
}

variable "versioning" {
  default = "false"
}

variable "name" {
  description = "the name of your cf-stack, e.g. \"devcf\""
}

variable "environment" {
  description = "the name of your environment, e.g. \"staging\""
}

variable "key_name" {
  description = "the name of the ssh key to use, e.g. \"stagingkey\""
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
  default     = "ap-southeast-2"
}

variable "availability_zones" {
  description = "a comma-separated list of availability zones, defaults to all AZ of the region, if set to something other than the defaults, both internal_subnets and external_subnets have to be defined as well"
  default     = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
}

variable "cidr" {
  description = "the CIDR block to provision for the VPC, if set to something other than the default, both internal_subnets and external_subnets have to be defined as well"
  default     = "10.30.0.0/16"
}

variable "private_subnets" {
  description = "a list of CIDRs for the private subnets in your VPC, must be set if the cidr variable is defined, needs to have as many elements as there are availability zones"
  default     = ["10.30.0.0/20", "10.30.96.0/20", "10.30.192.0/20"]
}

variable "services_subnets" {
  description = "a list of CIDRs for internal services subnets in your VPC, must be set if the cidr variable is defined, needs to have as many elements as there are availability zones"
  default     = ["10.30.16.0/20", "10.30.112.0/20", "10.30.208.0/20"]
}

variable "rds_subnets" {
  description = "a list of CIDRs for rds subnets in your VPC, must be set if the cidr variable is defined, needs to have as many elements as there are availability zones"
  default     = ["10.30.33.0/24", "10.30.129.0/24", "10.30.225.0/24"]
}

variable "external_subnets" {
  description = "a list of CIDRs for external subnets in your VPC, must be set if the cidr variable is defined, needs to have as many elements as there are availability zones"
  default     = ["10.30.32.0/24", "10.30.128.0/24", "10.30.224.0/24"]
}

variable "bastion_instance_type" {
  description = "Instance type for the bastion"
  default     = "t2.micro"
}

module "defaults" {
  source = "./modules/defaults"
  region = "${var.region}"
  cidr   = "${var.cidr}"
}

module "vpc" {
  source             = "./modules/vpc"
  name               = "${var.name}"
  cidr               = "${var.cidr}"
  private_subnets    = "${var.private_subnets}"
  services_subnets   = "${var.services_subnets}"
  rds_subnets        = "${var.rds_subnets}"
  external_subnets   = "${var.external_subnets}"
  availability_zones = "${var.availability_zones}"
  environment        = "${var.environment}"
}

module "security_groups" {
  source      = "./modules/security-groups"
  name        = "${var.name}"
  vpc_id      = "${module.vpc.id}"
  environment = "${var.environment}"
  cidr        = "${var.cidr}"
}

module "bastion" {
  source          = "./modules/bastion"
  region          = "${var.region}"
  instance_type   = "${var.bastion_instance_type}"
  security_groups = "${module.security_groups.external_ssh},${module.security_groups.internal_ssh}"
  vpc_id          = "${module.vpc.id}"
  subnet_id       = "${element(module.vpc.external_subnets, 0)}"
  key_name        = "${var.key_name}"
  environment     = "${var.environment}"
}

module "s3_bucket" "bucket" {
  source     = "./modules/s3_bucket"
  bucket     = "ausdto-${var.name}-${var.environment}-bosh-artifacts"
  acl        = "${var.acl}"
  versioning = false
}

// The region in which the infra lives.
output "aws_region" {
  value = "${var.region}"
}

// The bastion host IP.
output "bastion_ip" {
  value = "${module.bastion.external_ip}"
}

output "bucket id" {
  value = "${module.s3_bucket.id}"
}

output "bucket arn" {
  value = "${module.s3_bucket.arn}"
}

// Security group for external ELBs.
output "external_elb" {
  value = "${module.security_groups.external_elb}"
}

// Comma separated list of private subnet IDs.
output "private_subnets" {
  value = "${module.vpc.private_subnets}"
}

// Private Subnet CIDRs
output "private_subnet_cidrs" {
  value = "${var.private_subnets}"
}

// Comma separated list of services subnet IDs.
output "services_subnets" {
  value = "${module.vpc.services_subnets}"
}

// Comma separated list of rds subnet IDs.
output "rds_subnets" {
  value = "${module.vpc.rds_subnets}"
}

// Comma separated list of external subnet IDs.
output "external_subnets" {
  value = "${module.vpc.external_subnets}"
}

// The environment of the stack, e.g "prod".
output "environment" {
  value = "${var.environment}"
}

// The VPC availability zones.
output "availability_zones" {
  value = "${module.vpc.availability_zones}"
}

// The VPC security group ID.
output "vpc_security_group" {
  value = "${module.vpc.security_group}"
}

// The VPC ID.
output "vpc_id" {
  value = "${module.vpc.id}"
}

// Comma separated list of internal route table IDs.
output "internal_route_tables" {
  value = "${module.vpc.internal_rtb_id}"
}

// The external route table ID.
output "external_route_tables" {
  value = "${module.vpc.external_rtb_id}"
}

output "bosh_az" {
  value = "${element(var.availability_zones,0)}"
}

output "bosh_subnet_ip" {
  value = "${cidrhost(element(var.private_subnets,0),6)}"
}

output "bosh_subnet_gateway" {
  value = "${cidrhost(element(var.private_subnets,0),1)}"
}

output "bosh_subnet_dns" {
  value = "${cidrhost(element(var.private_subnets,0),2)}"
}
